from http.server import BaseHTTPRequestHandler, HTTPServer

import os
import socket
from time import sleep

"""
GetBlue Android parameters
Host: http://IP_DU_PC:8080
Parameter for data: data
Parameter for timestamp: timestamp
HTTP request: GET
"""


class Server(BaseHTTPRequestHandler):
    def _set_headers(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def do_GET(self):
        self._set_headers()
        isbn = self.path[7:-24]
        if not isbn.isnumeric():
            print("Mauvais ISBN.")
            return
        print("Hey j'ai un ISBN :", isbn)
        os.system("xdotool type " + isbn)
        os.system("xdotool key KP_Enter")
        sleep(1)
        os.system("xdotool click 1")

    def do_HEAD(self):
        self._set_headers()


class HTTPServerV6(HTTPServer):
    address_family = socket.AF_INET6


if __name__ == "__main__":
    server_address = ('::', 8080)
    httpd = HTTPServerV6(server_address, Server)
    print('Starting httpd...')
    httpd.serve_forever()
