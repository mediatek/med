# -*- mode: python; coding: utf-8 -*-
# Copyright (C) 2017-2019 by BDE ENS Paris-Saclay
# SPDX-License-Identifier: GPL-3.0-or-later

from django.conf.urls import url

from . import views

app_name = 'logs'
urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^stats_actions/$', views.stats_actions, name='stats-actions'),
]
