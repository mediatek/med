# -*- mode: python; coding: utf-8 -*-
# Copyright (C) 2017-2019 by BDE ENS Paris-Saclay
# SPDX-License-Identifier: GPL-3.0-or-later

from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.decorators import login_required
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.db.models import Count
from django.shortcuts import render
from django.utils.translation import gettext_lazy as _
from reversion.models import Revision
from med.settings import PAGINATION_NUMBER
from users.models import User


@login_required
@staff_member_required
def index(request):
    revisions = Revision.objects.all().order_by(
        'date_created').reverse().select_related('user').prefetch_related(
        'version_set__object')
    paginator = Paginator(revisions, PAGINATION_NUMBER)
    page = request.GET.get('page')
    try:
        revisions = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        revisions = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        revisions = paginator.page(paginator.num_pages)
    return render(request, 'logs/index.html', {'revisions_list': revisions})


@login_required
@staff_member_required
def stats_actions(request):
    stats = User.objects.annotate(num=Count('revision')).order_by('-num')[:10]
    return render(request, 'logs/stats_users.html', {
        'title': _("Database edits by user"),
        'stats': stats,
    })
