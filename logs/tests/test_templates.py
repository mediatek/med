# -*- mode: python; coding: utf-8 -*-
# SPDX-License-Identifier: GPL-3.0-or-later

from django.test import TestCase
from django.urls import reverse
from users.models import User

"""
Test that every page render
"""


class TemplateTests(TestCase):
    def setUp(self):
        self.user = User.objects.create_superuser(
            username="admin",
            password="adminadmin",
            email="admin@example.com",
        )
        self.client.force_login(self.user)

    def test_logs_index(self):
        response = self.client.get(reverse("logs:index"))
        self.assertEqual(response.status_code, 200)

    def test_logs_stats_actions(self):
        response = self.client.get(reverse("logs:stats-actions"))
        self.assertEqual(response.status_code, 200)
