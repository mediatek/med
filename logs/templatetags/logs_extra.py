# -*- mode: python; coding: utf-8 -*-
# Copyright (C) 2017-2019 by BDE ENS Paris-Saclay
# SPDX-License-Identifier: GPL-3.0-or-later

from django import template

register = template.Library()


@register.filter
def classname(obj):
    return obj.__class__.__name__
