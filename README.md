# Site de la Mediatek

[![License: GPL v3](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0.txt)
[![pipeline status](https://gitlab.crans.org/mediatek/med/badges/master/pipeline.svg)](https://gitlab.crans.org/mediatek/med/commits/master)
[![coverage report](https://gitlab.crans.org/mediatek/med/badges/master/coverage.svg)](https://gitlab.crans.org/mediatek/med/commits/master)

Le projet Med permet la gestion de la base de donnée de la médiathèque de l'ENS Paris-Saclay.
Elle permet de gérer les medias, bd, jeux, emprunts, ainsi que les adhérents de la med.

## Licence 

Ce projet est sous la licence GNU public license v3.0.

## Installation

### Développement

On peut soit développer avec Docker, soit utiliser un VirtualEnv.

Dans le cas du VirtualEnv,

```bash
python3 -m venv venv
. venv/bin/activate
pip install -r requirements.txt
./manage.py compilemessages
./manage.py makemigrations
./manage.py migrate
./manage.py runserver
```

### Production

Vous pouvez soit utiliser Docker, soit configurer manuellement le serveur.

#### Mise en place du projet sur Zamok

Pour mettre en place le projet sans droits root,
on va créer un socket uwsgi dans le répertoire personnel de l'utilisateur `club-med`
puis on va dire à Apache2 d'utiliser ce socket avec un `.htaccess`.

```bash
git clone https://gitlab.crans.org/mediatek/med.git django-med
chmod go-rwx -R django-med
python3 -m venv venv --system-site-packages
. venv/bin/activate
pip install -r requirements.txt
pip install mysqlclient~=1.4.4  # si base MySQL
pip install uwsgi~=2.0.18  # si production
./entrypoint.sh  # lance en shell
```

Pour lancer le serveur au démarrage de Zamok, on suit les instructions dans `django-med.service`.

Pour reverse-proxyfier le serveur derrière Apache, on place dans `~/www/.htaccess` :

```apache
RewriteEngine On

# UWSGI socket
RewriteRule ^django.wsgi/(.*)$ unix:/home/c/club-med/django-med/uwsgi.sock|fcgi://localhost/ [P,NE,L]

# When not a file and not starting with django.wsgi, then forward to UWSGI
RewriteCond %{REQUEST_URI} !^/django.wsgi/
RewriteCond %{REQUEST_FILENAME} !-f
RewriteRule ^(.*)$ /django.wsgi/$1 [QSA,L]
```

Il est néanmoins une mauvaise idée de faire de la production sur SQLite,
on configure donc ensuite Django et une base de données.

#### Configuration d'une base de données

Sur le serveur MySQL ou PostgreSQL, il est nécessaire de créer une base de donnée med,
ainsi qu'un user med et un mot de passe associé.

Voici les étapes à executer pour PostgreSQL :

```SQL
CREATE DATABASE "club-med";
CREATE USER "club-med" WITH PASSWORD 'MY-STRONG-PASSWORD';
GRANT ALL PRIVILEGES ON DATABASE "club-med" TO "club-med";
```

## Exemple de groupes de droits

```
bureau
    media | Can view borrowed item
    media | Can add borrowed item
    media | Can change borrowed item
    media | Can delete borrowed item
    users | Can view user
    users | Can add user
    users | Can change user
    sporz | Can view gamesave
    + permissions keyholder

keyholder
    media | Can view author
    media | Can add author
    media | Can change author
    media | Can delete author
    media | Can view medium
    media | Can add medium
    media | Can change medium
    media | Can delete medium
    media | Can view game
    media | Can add game
    media | Can change game
    media | Can delete game
    media | Can view borrowed item
    media | Can add borrowed item
    media | Can change borrowed item
    media | Can delete borrowed item
    users | Can view user

users (default group for everyone)
    media | Can view author
    media | Can view game
    media | Can view medium
    sporz | Can add gamesave
    sporz | Can change gamesave
    sporz | Can add player
    sporz | Can change player
    sporz | Can delete player
    sporz | Can view player
```
