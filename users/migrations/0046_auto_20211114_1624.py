# Generated by Django 2.2.24 on 2021-11-14 15:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0045_auto_20211114_1423'),
    ]

    operations = [
        migrations.AlterField(
            model_name='membership',
            name='date_end',
            field=models.DateField(verbose_name='start date'),
        ),
        migrations.AlterField(
            model_name='membership',
            name='date_start',
            field=models.DateField(verbose_name='start date'),
        ),
    ]
