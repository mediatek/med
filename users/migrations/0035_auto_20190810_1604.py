# Generated by Django 2.2.4 on 2019-08-10 14:04

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0034_auto_20190810_1604'),
    ]

    operations = [
        migrations.RenameField(
            model_name='adhesion',
            old_name='adherent',
            new_name='members',
        ),
    ]
