# -*- mode: python; coding: utf-8 -*-
# Copyright (C) 2017-2019 by BDE ENS Paris-Saclay
# SPDX-License-Identifier: GPL-3.0-or-later

from django.apps import AppConfig
from django.conf import settings
from django.db.models.signals import post_save
from django.utils.translation import ugettext_lazy as _

from .signals import add_to_default_group


class UsersConfig(AppConfig):
    name = 'users'
    verbose_name = _('users')

    def ready(self):
        post_save.connect(add_to_default_group,
                          sender=settings.AUTH_USER_MODEL)
