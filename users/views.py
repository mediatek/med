# -*- mode: python; coding: utf-8 -*-
# Copyright (C) 2017-2019 by BDE ENS Paris-Saclay
# SPDX-License-Identifier: GPL-3.0-or-later
from datetime import datetime

from authlib.integrations.django_client import OAuth
from django.contrib.auth import login
from django.contrib.auth.models import Group
from django.urls import reverse
from django.utils import timezone
from django.views.generic import RedirectView
from rest_framework import viewsets
from users.models import User, AccessToken

from .serializers import GroupSerializer, UserSerializer


class LoginView(RedirectView):
    def get_redirect_url(self, *args, **kwargs):
        oauth = OAuth()
        oauth.register('notekfet')
        redirect_url = self.request.build_absolute_uri(reverse('users:auth'))
        return oauth.notekfet.authorize_redirect(self.request,
                                                 redirect_url).url


class AuthorizeView(RedirectView):
    def get_redirect_url(self, *args, **kwargs):
        oauth = OAuth()
        oauth.register('notekfet')
        token = oauth.notekfet.authorize_access_token(self.request)
        token_obj = AccessToken.objects.create(
            access_token=token['access_token'],
            expires_in=token['expires_in'],
            scopes=token['scope'],
            refresh_token=token['refresh_token'],
            expires_at=timezone.utc.fromutc(
                datetime.fromtimestamp(token['expires_at'])),
        )
        user = token_obj.fetch_user(True)
        self.request.session['access_token_id'] = token_obj.id
        self.request.session.save()
        login(self.request, user)
        return reverse('index')


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer
