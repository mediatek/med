# -*- mode: python; coding: utf-8 -*-
# Copyright (C) 2017-2019 by BDE ENS Paris-Saclay
# SPDX-License-Identifier: GPL-3.0-or-later

from django.conf.urls import url

from . import views

app_name = 'users'
urlpatterns = [
    url('login/', views.LoginView.as_view(), name='login'),
    url('authorize/', views.AuthorizeView.as_view(), name='auth'),
]
