# -*- mode: python; coding: utf-8 -*-
# Copyright (C) 2017-2019 by BDE ENS Paris-Saclay
# SPDX-License-Identifier: GPL-3.0-or-later

from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.utils import timezone
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext_lazy as _
from reversion.admin import VersionAdmin
from med.admin import admin_site

from .models import User


class IsMemberFilter(admin.SimpleListFilter):
    title = _('membership status')
    parameter_name = 'is_member'

    def lookups(self, request, model_admin):
        return (
            ('Yes', _('Yes')),
        )

    def queryset(self, request, queryset):
        if self.parameter_name in request.GET:
            queryset = queryset.filter(
                membership__date_start__lte=timezone.now(),
                membership__date_end__gte=timezone.now(),
            ).distinct()

        return queryset


class UserAdmin(VersionAdmin, BaseUserAdmin):
    # Customize admin to add more fields
    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        (_('Personal info'), {'fields': ('first_name', 'last_name', 'email',
                                         'phone_number', 'address',
                                         'comment')}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
    list_display = ('username', 'email', 'first_name', 'last_name',
                    'is_member', 'is_staff')
    list_filter = (IsMemberFilter, 'is_staff', 'is_superuser', 'is_active',
                   'groups')

    def has_add_permission(self, request):
        # Only add users through Note Kfet login
        return False

    def is_member(self, obj):
        """
        Get current membership year and check if user is there
        """
        if obj.is_member:
            return mark_safe(
                '<img src="/static/admin/img/icon-yes.svg" alt="True">'
            )
        else:
            return mark_safe(
                '<img src="/static/admin/img/icon-no.svg" alt="False">'
            )

    is_member.short_description = _('is member')
    is_member.allow_tags = True


admin_site.register(User, UserAdmin)
