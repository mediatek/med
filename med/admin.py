# -*- mode: python; coding: utf-8 -*-
# Copyright (C) 2017-2019 by BDE ENS Paris-Saclay
# SPDX-License-Identifier: GPL-3.0-or-later

from django.contrib.admin import AdminSite
from django.contrib.auth.admin import Group, GroupAdmin
from django.contrib.sites.admin import Site, SiteAdmin
from django.utils.translation import gettext_lazy as _
from django.views.decorators.cache import never_cache
from media.models import Borrow


class DatabaseAdmin(AdminSite):
    index_title = _('Welcome to the Mediatek database')

    @never_cache
    def index(self, request, extra_context=None):
        """
        Add borrowed item to admin index
        """
        response = super().index(request, extra_context)

        # User is always authenticated
        # Get currently borrowed items
        user_borrowed = Borrow.objects.filter(user=request.user,
                                              given_back=None)
        response.context_data["borrowed_items"] = user_borrowed

        return response

    def has_permission(self, request):
        """
        Authorize all active user to access admin
        """
        return request.user.is_active


# Instantiate admin site and register some defaults
admin_site = DatabaseAdmin()
admin_site.register(Group, GroupAdmin)
admin_site.register(Site, SiteAdmin)
