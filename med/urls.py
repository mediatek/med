# -*- mode: python; coding: utf-8 -*-
# Copyright (C) 2017-2019 by BDE ENS Paris-Saclay
# SPDX-License-Identifier: GPL-3.0-or-later

from django.contrib.auth.views import PasswordResetView
from django.urls import include, path
from django.views.generic import RedirectView
from rest_framework import routers

import media.views
import users.views
from .admin import admin_site

# API router
router = routers.DefaultRouter()
router.register(r'authors', media.views.AuthorViewSet)
router.register(r'media/comic', media.views.ComicViewSet)
router.register(r'media/manga', media.views.MangaViewSet)
router.register(r'media/cd', media.views.CDViewSet)
router.register(r'media/vinyl', media.views.VinylViewSet)
router.register(r'media/novel', media.views.NovelViewSet)
router.register(r'media/review', media.views.ReviewViewSet)
router.register(r'media/future', media.views.FutureMediumViewSet)
router.register(r'borrowed_items', media.views.BorrowViewSet)
router.register(r'games', media.views.GameViewSet)
router.register(r'users', users.views.UserViewSet)
router.register(r'groups', users.views.GroupViewSet)

urlpatterns = [
    path('', media.views.IndexView.as_view(), name='index'),

    # Include project routers
    path('users/', include('users.urls')),
    path('media/', include('media.urls')),
    path('logs/', include('logs.urls')),

    # REST API
    path('api/', include(router.urls)),
    path('api-auth/', include('rest_framework.urls')),

    # Include Django Contrib and Core routers
    path('accounts/password_reset/', PasswordResetView.as_view(),
         name='admin_password_reset'),
    path('i18n/', include('django.conf.urls.i18n')),
    path('accounts/', include('django.contrib.auth.urls')),
    path('accounts/profile/', RedirectView.as_view(pattern_name='index')),
    path('database/doc/', include('django.contrib.admindocs.urls')),
    path('database/', admin_site.urls),
]
