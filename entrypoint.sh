#!/bin/bash
# This will launch the Django project as a fastcgi socket
# then Apache or NGINX will be able to use that socket

# Option "-i" will be only available in Django 3.0+, but it does not support Python 3.5
#python manage.py compilemessages -i ".tox" -i "venv"
python manage.py compilemessages

# Wait for database (docker)
sleep 2

python manage.py migrate
python manage.py collectstatic --no-input

# harakiri parameter respawns processes taking more than 20 seconds
# max-requests parameter respawns processes after serving 5000 requests
# vacuum parameter cleans up when stopped
uwsgi --socket "$HOME/www/uwsgi.sock" --chmod-socket=666 --master --plugins python3 \
      --module med.wsgi:application --env DJANGO_SETTINGS_MODULE=med.settings \
      --processes 4 --harakiri=20 --max-requests=5000 --vacuum \
      --static-map /static="$(pwd)/static" --protocol=fastcgi
