from time import sleep

from django.core.management import BaseCommand
from media.forms import MediaAdminForm
from media.models import Comic, Manga


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('--view-only', action="store_true",
                            help="Display only modifications. "
                                 + "Only useful for debug.")

    def handle(self, *args, **options):
        converted = 0

        for media in Comic.objects.all():
            if media.pk < 3400:
                continue
            # We sleep 5 seconds to avoid a ban from Bedetheque
            sleep(5)
            self.stdout.write(str(media))
            form = MediaAdminForm(instance=media,
                                  data={"isbn": media.isbn, "_isbn": True, })
            form.full_clean()

            if "format" not in form.cleaned_data:
                self.stdout.write("Format not specified."
                                  " Assume it is a comic strip.")
                continue

            format = form.cleaned_data["format"]
            self.stdout.write("Format: {}".format(format))

            if not options["view_only"]:
                if format == "manga":
                    self.stdout.write(self.style.WARNING(
                        "This media is a manga. "
                        "Transfer it into a new object..."))
                    manga = Manga.objects.create(
                        isbn=media.isbn,
                        title=media.title,
                        subtitle=media.subtitle,
                        external_url=media.external_url,
                        side_identifier=media.side_identifier,
                        number_of_pages=media.number_of_pages,
                        publish_date=media.publish_date,
                    )

                    manga.authors.set(media.authors.all())
                    manga.save()

                    self.stdout.write(self.style.SUCCESS(
                        "Manga successfully saved. Deleting old medium..."))

                    media.delete()
                    self.stdout.write(self.style.SUCCESS("Medium deleted"))

                    converted += 1
        self.stdout.write(self.style.SUCCESS(
            "Successfully saved {:d} mangas".format(converted)))
