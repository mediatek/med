from argparse import FileType
from sys import stdin

from django.core.management import BaseCommand
from media.forms import generate_side_identifier
from media.models import Novel, Author


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('input', nargs='?',
                            type=FileType('r'),
                            default=stdin,
                            help="Revues to be imported.")

    def handle(self, *args, **options):
        file = options["input"]
        romans = []
        for line in file:
            romans.append(line[:-1].split('|'))

        print("Registering", len(romans), "romans")

        imported = 0

        for book in romans:
            if len(book) != 2:
                continue

            title = book[1]
            authors = [Author.objects.get_or_create(name=n)[0]
                       for n in book[0].split(';')]
            side_identifier = generate_side_identifier(title, authors)
            roman = Novel.objects.create(
                title=title,
                side_identifier=side_identifier,
            )
            roman.authors.set(authors)
            roman.save()

            self.stdout.write(self.style.SUCCESS(
                "Roman imported"))
            imported += 1

        self.stdout.write(self.style.SUCCESS(
            "{count} romans imported".format(count=imported)))
