import os.path

from django.core.management import BaseCommand

from media.models import Comic, CD, Manga, Review, Novel, Vinyl, Game


class Command(BaseCommand):
    """
    Extract the database into a user-friendly website written in Markdown.
    """
    def add_arguments(self, parser):
        parser.add_argument('--directory', '-d', type=str, default='.',
                            help="Directory where mkdocs is running.")

    def handle(self, *args, **options):
        directory = options["directory"]

        with open(os.path.join(directory, 'docs', 'index.md'), "w") as f:
            f.write("# Media de la Mediatek\n\n\n")
            f.write("Ce site répertorie l'intégralité des media présents "
                    "à la Mediatek de l'ENS Paris-Saclay.\n")

        for model_class, file_name in [(Manga, "mangas.md"),
                                       (Novel, "romans.md"),
                                       (CD, "cd.md"), (Vinyl, "vinyles.md")]:
            self.process_model_class(model_class, file_name, f, directory)

        # Traitement différent pour les revues
        with open(os.path.join(directory, 'docs', 'revues.md'), "w") as f:
            f.write("# Revues\n\n\n")

            titles = list(set(obj["title"] for obj in
                              Review.objects.values("title").distinct().all()))
            titles.sort()

            for title in titles:
                f.write(f"## {title}\n\n\n")

                for medium in Review.objects.filter(title=title)\
                        .order_by("number").all():
                    f.write(f"### Numéro {medium.number}\n\n\n")
                    if medium.double:
                        f.write("Double revue\n\n")
                    if medium.year:
                        f.write(f"Année : {medium.year}\n\n")
                    if medium.month:
                        f.write(f"Mois : {medium.month}\n\n")
                    if medium.day:
                        f.write(f"Jour : {medium.day}\n\n")
                    f.write("\n\n\n")

        # Traitement différent pour les jeux
        with open(os.path.join(directory, 'docs', 'jeux.md'), "w") as f:
            f.write("# Jeux\n\n\n")

            for game in Game.objects.order_by("title").all():
                f.write(f"## {game.title}\n\n\n")
                if hasattr(medium, "isbn"):
                    f.write(f"ISBN : {game.isbn}\n\n")
                f.write(f"Durée : {game.duration}\n\n")
                f.write(f"Nombre de joueurs : {game.players_min} "
                        f"- {game.players_max}\n\n")
                if game.owner.username != "Med":
                    f.write(f"Propriétaire : {game.owner.username}\n\n")
                if game.comment:
                    f.write(f"Commentaire : {game.comment}\n\n")
                f.write("\n\n\n")

        # Traitement différent pour les bds
        # Regroupement par le premier caractère
        titles = list(set(obj["title"] for obj in Comic.objects
                          .values("title").distinct().all()))
        category = lambda s: s[0].lower() if 'a' <= s[0].lower() <= 'z' else '#'
        db = {}
        for title in titles:
            c = category(title)
            if c not in db:
                db[c] = []
            db[c].append(title)

        for letter, titles in db.items():
            with open(os.path.join(directory, 'docs', 'bds', f'{letter}.md'), 'w') as f:
                f.write("# " + str(Comic._meta.verbose_name_plural)
                        .capitalize() + f" - {letter.upper()}\n\n\n")

                titles.sort()

                for title in titles:
                    f.write(f"## {title}\n\n\n")

                    for medium in Comic.objects.filter(title=title) \
                            .order_by("side_identifier").all():
                        if hasattr(medium, "subtitle"):
                            f.write(f"### {medium.subtitle}\n\n\n")
                        if hasattr(medium, "isbn"):
                            f.write(f"ISBN : {medium.isbn}\n\n")
                        f.write(f"Cote : {medium.side_identifier}\n\n")
                        f.write("Auteurs : " + ", ".join(
                            author.name for author in medium.authors.all())
                            + "\n\n")
                        if hasattr(medium, "number_of_pages"):
                            f.write(f"Nombre de pages : "
                                    f"{medium.number_of_pages}\n\n")
                        if hasattr(medium, "rpm"):
                            f.write(f"Tours par minute : "
                                    f"{medium.rpm}\n\n")
                        if hasattr(medium, "publish_date"):
                            f.write(f"Publié le : "
                                    f"{medium.publish_date}\n\n")
                        if hasattr(medium, "external_url"):
                            f.write(f"Lien : [{medium.external_url}]"
                                    f"({medium.external_url})\n\n")
                        f.write("\n\n\n")

    def process_model_class(self, model_class, file_name, f, directory):
        with open(os.path.join(directory, 'docs', file_name), "w") as f:
            f.write("# " + str(model_class._meta.verbose_name_plural)
                    .capitalize() + "\n\n\n")

            titles = list(set(obj["title"] for obj in model_class.objects
                              .values("title").distinct().all()))
            titles.sort()

            for title in titles:
                f.write(f"## {title}\n\n\n")

                for medium in model_class.objects.filter(title=title) \
                        .order_by("side_identifier").all():
                    if hasattr(medium, "subtitle"):
                        f.write(f"### {medium.subtitle}\n\n\n")
                    if hasattr(medium, "isbn"):
                        f.write(f"ISBN : {medium.isbn}\n\n")
                    f.write(f"Cote : {medium.side_identifier}\n\n")
                    f.write("Auteurs : " + ", ".join(
                        author.name for author in medium.authors.all())
                        + "\n\n")
                    if hasattr(medium, "number_of_pages"):
                        f.write(f"Nombre de pages : "
                                f"{medium.number_of_pages}\n\n")
                    if hasattr(medium, "rpm"):
                        f.write(f"Tours par minute : "
                                f"{medium.rpm}\n\n")
                    if hasattr(medium, "publish_date"):
                        f.write(f"Publié le : "
                                f"{medium.publish_date}\n\n")
                    if hasattr(medium, "external_url"):
                        f.write(f"Lien : [{medium.external_url}]"
                                f"({medium.external_url})\n\n")
                    f.write("\n\n\n")
