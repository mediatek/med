from random import random
from time import sleep

from django.core.management import BaseCommand
from media.forms import MediaAdminForm
from media.models import Comic, FutureMedium, Manga, Novel


class Command(BaseCommand):
    def handle(self, *args, **options):
        for future_medium in FutureMedium.objects.all():
            isbn = future_medium.isbn
            type_str = future_medium.type
            if type_str == 'bd':
                cl = Comic
            elif type_str == 'manga':
                cl = Manga
            elif type_str == 'roman':
                cl = Novel
            else:
                self.stderr.write(self.style.WARNING(
                    "Unknown medium type: {type}. Ignoring..."
                    .format(type=type_str)))
                continue

            if cl.objects.filter(isbn=isbn).exists():
                self.stderr.write(self.style.WARNING(
                    f"ISBN {isbn} for type {type_str} already exists, "
                    f"remove it"))
                future_medium.delete()
                continue

            form = MediaAdminForm(instance=cl(),
                                  data={"isbn": isbn, "_isbn": True, })
            # Don't DDOS any website
            sleep(5 + (4 * random() - 1))

            try:
                form.full_clean()
                if hasattr(form.instance, "subtitle") and \
                        not form.instance.subtitle:
                    form.instance.subtitle = ""
                form.save()
                future_medium.delete()
                self.stdout.write(self.style.SUCCESS(
                    "Medium with ISBN {isbn} successfully imported"
                    .format(isbn=isbn)))
            except Exception as e:
                self.stderr.write(self.style.WARNING(
                    "An error occured while importing ISBN {isbn}: {error}"
                    .format(isbn=isbn,
                            error=str(e.__class__) + "(" + str(e) + ")")))
