from argparse import FileType
from sys import stdin

from django.core.management import BaseCommand
from media.models import Author, Comic


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('input', nargs='?',
                            type=FileType('r'),
                            default=stdin,
                            help="Marvel comic to be imported.")

    def handle(self, *args, **options):
        file = options["input"]
        revues = []
        for line in file:
            revues.append(line[:-1].split('|', 2))

        print("Registering", len(revues), "Marvel comics")

        imported = 0

        for revue in revues:
            if len(revue) != 3:
                continue

            title = revue[0]
            number = revue[1]
            authors = [Author.objects.get_or_create(name=n)[0]
                       for n in revue[2].split('|')]
            bd = Comic.objects.create(
                title=title,
                subtitle=number,
                side_identifier="{:.3} {:.3} {:0>2}"
                                .format(authors[0].name.upper(),
                                        title.upper(),
                                        number),
            )

            bd.authors.set(authors)
            bd.save()

            self.stdout.write(self.style.SUCCESS(
                "Comic imported"))
            imported += 1

        self.stdout.write(self.style.SUCCESS(
            "{count} comics imported".format(count=imported)))
