from argparse import FileType
from sys import stdin

from django.core.management import BaseCommand
from media.models import Author, Vinyl


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('input', nargs='?',
                            type=FileType('r'),
                            default=stdin,
                            help="Vinyle to be imported.")

        parser.add_argument('--rpm',
                            type=int,
                            default=45,
                            help="RPM of the imported vinyles.")

    def handle(self, *args, **options):
        rpm = options["rpm"]
        file = options["input"]
        vinyles = []
        for line in file:
            vinyles.append(line[:-1].split('|', 2))

        print("Registering", len(vinyles), "vinyles")

        imported = 0

        for vinyle in vinyles:
            if len(vinyle) != 3:
                continue

            side = vinyle[0]
            title = vinyle[1 if rpm == 33 else 2]
            authors_str = vinyle[2 if rpm == 33 else 1]\
                .split('|' if rpm == 33 else ';')
            authors = [Author.objects.get_or_create(name=author)[0]
                       for author in authors_str]
            vinyle, created = Vinyl.objects.get_or_create(
                title=title,
                side_identifier=side,
                rpm=rpm,
            )
            vinyle.authors.set(authors)
            vinyle.save()

            if not created:
                self.stderr.write(self.style.WARNING(
                    "One vinyle was already imported. Skipping..."))
            else:
                self.stdout.write(self.style.SUCCESS(
                    "Vinyle imported"))
                imported += 1

        self.stdout.write(self.style.SUCCESS(
            "{count} vinyles imported".format(count=imported)))
