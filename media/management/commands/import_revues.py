from argparse import FileType
from sys import stdin

from django.core.management import BaseCommand
from media.models import Review


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('input', nargs='?',
                            type=FileType('r'),
                            default=stdin,
                            help="Revues to be imported.")

    def handle(self, *args, **options):
        file = options["input"]
        revues = []
        for line in file:
            revues.append(line[:-1].split('|'))

        print("Registering", len(revues), "revues")

        imported = 0

        for revue in revues:
            if len(revue) != 5:
                continue

            title = revue[0]
            number = revue[1]
            day = revue[2]
            if not day:
                day = None
            month = revue[3]
            if not month:
                month = None
            year = revue[4]
            if not year:
                year = None
            revue, created = Review.objects.get_or_create(
                title=title,
                number=number.replace('*', ''),
                year=year,
                month=month,
                day=day,
                double=number.endswith('*'),
            )

            if not created:
                self.stderr.write(self.style.WARNING(
                    "One revue was already imported. Skipping..."))
            else:
                self.stdout.write(self.style.SUCCESS(
                    "Revue imported"))
                imported += 1

        self.stdout.write(self.style.SUCCESS(
            "{count} revues imported".format(count=imported)))
