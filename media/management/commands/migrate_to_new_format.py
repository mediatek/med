from django.core.management import BaseCommand
from django.db import transaction
from django.utils.translation import gettext_lazy as _
from media.models import CD, Comic, Game, Manga, Novel, Review, Vinyl
from tqdm import tqdm


class Command(BaseCommand):
    """
    Convert old format into new format
    """

    def add_arguments(self, parser):
        parser.add_argument('--doit', action='store_true',
                            help="Actually do the mogration.")

    @transaction.atomic
    def handle(self, *args, **options):  # noqa: C901
        self.stderr.write(self.style.WARNING(
            "Old data structure has been deleted. This script won't work "
            "anymore (and is now useless)"))

        from media.models import OldCD, OldComic, OldGame, OldManga, OldNovel,\
            OldReview, OldVinyl

        # Migrate books
        for old_book_class, book_class in [(OldComic, Comic),
                                           (OldManga, Manga),
                                           (OldNovel, Novel)]:
            name = book_class._meta.verbose_name
            name_plural = book_class._meta.verbose_name_plural
            for book in tqdm(old_book_class.objects.all(),
                             desc=name_plural, unit=str(name)):
                try:
                    new_book = book_class.objects.create(
                        isbn=book.isbn,
                        title=book.title,
                        subtitle=book.subtitle,
                        external_url=book.external_url,
                        side_identifier=book.side_identifier,
                        number_of_pages=book.number_of_pages,
                        publish_date=book.publish_date,
                        present=book.present,
                    )
                    new_book.authors.set(book.authors.all())
                    new_book.save()
                except Exception:
                    self.stderr.write(f"There was an error with {name} "
                                      f"{book} ({book.pk})")
                    raise

            self.stdout.write(f"{book_class.objects.count()} {name_plural} "
                              "migrated")

        # Migrate CDs
        for cd in tqdm(OldCD.objects.all(),
                       desc=_("CDs"), unit=str(_("CD"))):
            try:
                new_cd = CD.objects.create(
                    title=cd.title,
                    present=cd.present,
                )
                new_cd.authors.set(cd.authors.all())
                new_cd.save()
            except Exception:
                self.stderr.write(f"There was an error with {cd} ({cd.pk})")
                raise

        self.stdout.write(f"{CD.objects.count()} {_('CDs')} migrated")

        # Migrate vinyls
        for vinyl in tqdm(OldVinyl.objects.all(),
                          desc=_("vinyls"), unit=str(_("vinyl"))):
            try:
                new_vinyl = Vinyl.objects.create(
                    title=vinyl.title,
                    present=vinyl.present,
                    rpm=vinyl.rpm,
                )
                new_vinyl.authors.set(vinyl.authors.all())
                new_vinyl.save()
            except Exception:
                self.stderr.write(f"There was an error with {vinyl} "
                                  f"({vinyl.pk})")
                raise

        self.stdout.write(f"{Vinyl.objects.count()} {_('vinyls')} migrated")

        # Migrate reviews
        for review in tqdm(OldReview.objects.all(),
                           desc=_("reviews"), unit=str(_("review"))):
            try:
                Review.objects.create(
                    title=review.title,
                    number=review.number,
                    year=review.year,
                    month=review.month,
                    day=review.day,
                    double=review.double,
                    present=review.present,
                )
            except Exception:
                self.stderr.write(f"There was an error with {review} "
                                  f"({review.pk})")
                raise

        self.stdout.write(f"{Review.objects.count()} {_('reviews')} migrated")

        # Migrate games
        for game in tqdm(OldGame.objects.all(),
                         desc=_("games"), unit=str(_("game"))):
            try:
                Game.objects.create(
                    title=game.title,
                    owner=game.owner,
                    duration=game.duration,
                    players_min=game.players_min,
                    players_max=game.players_max,
                    comment=game.comment,
                )
            except Exception:
                self.stderr.write(f"There was an error with {game} "
                                  f"({game.pk})")
                raise

        self.stdout.write(f"{Game.objects.count()} {_('games')} migrated")

        if not options['doit']:
            self.stdout.write(self.style.WARNING(
                "Warning: Data were't saved. Please use --doit option "
                "to really perform the migration."
            ))
            exit(1)
