# -*- mode: python; coding: utf-8 -*-
# Copyright (C) 2017-2019 by BDE ENS Paris-Saclay
# SPDX-License-Identifier: GPL-3.0-or-later

from django.utils.html import format_html
from django.utils.translation import ugettext_lazy as _
from polymorphic.admin import PolymorphicChildModelAdmin, \
    PolymorphicParentModelAdmin
from med.admin import admin_site
from reversion.admin import VersionAdmin

from .forms import MediaAdminForm
from .models import Author, Borrow, Borrowable, CD, Comic, FutureMedium, \
    Game, Manga, Novel, Review, Vinyl


class AuthorAdmin(VersionAdmin):
    list_display = ('name',)
    search_fields = ('name',)


class BorrowableAdmin(PolymorphicParentModelAdmin):
    search_fields = ('title',)
    child_models = (CD, Comic, Manga, Novel, Review, Vinyl,)

    def get_model_perms(self, request):
        # We don't want that the borrowable items appear directly in
        # main menu, but we still want search borrowable items.
        return {}


class MediumAdmin(VersionAdmin, PolymorphicChildModelAdmin):
    list_display = ('__str__', 'authors_list', 'side_identifier', 'isbn',
                    'external_link')
    search_fields = ('title', 'authors__name', 'side_identifier', 'subtitle',
                     'isbn')
    autocomplete_fields = ('authors',)
    date_hierarchy = 'publish_date'
    form = MediaAdminForm
    show_in_index = True

    def authors_list(self, obj):
        return ", ".join([a.name for a in obj.authors.all()])

    authors_list.short_description = _('authors')

    def external_link(self, obj):
        if obj.external_url:
            return format_html('<a href="{}" target="about:blank">{}</a>',
                               obj.external_url, obj.external_url)
        else:
            return "-"

    external_link.allow_tags = True
    external_link.short_description = _('external url')

    def get_form(self, request, obj=None, **kwargs):
        """
        Pass request to form (for ISBN magic)
        """
        form = super().get_form(request, obj=obj, **kwargs)
        form.request = request
        return form

    def changeform_view(self, request, object_id=None, form_url='',
                        extra_context=None):
        """
        We use _continue for ISBN fetching, so remove continue button
        """
        extra_context = extra_context or {}
        extra_context['show_save_and_continue'] = False
        return super().changeform_view(request, object_id, form_url,
                                       extra_context=extra_context)


class FutureMediumAdmin(VersionAdmin):
    list_display = ('isbn',)
    search_fields = ('isbn',)

    def changeform_view(self, request, object_id=None, form_url='',
                        extra_context=None):
        """
        We use _continue for ISBN fetching, so remove continue button
        """
        extra_context = extra_context or {}
        extra_context['show_save_and_continue'] = False
        extra_context['show_save'] = False
        return super().changeform_view(request, object_id, form_url,
                                       extra_context=extra_context)


class CDAdmin(VersionAdmin, PolymorphicChildModelAdmin):
    list_display = ('title', 'authors_list', 'side_identifier',)
    search_fields = ('title', 'authors__name', 'side_identifier',)
    autocomplete_fields = ('authors',)
    show_in_index = True

    def authors_list(self, obj):
        return ", ".join([a.name for a in obj.authors.all()])

    authors_list.short_description = _('authors')


class VinylAdmin(VersionAdmin, PolymorphicChildModelAdmin):
    list_display = ('title', 'authors_list', 'side_identifier', 'rpm',)
    search_fields = ('title', 'authors__name', 'side_identifier', 'rpm',)
    autocomplete_fields = ('authors',)
    show_in_index = True

    def authors_list(self, obj):
        return ", ".join([a.name for a in obj.authors.all()])

    authors_list.short_description = _('authors')


class ReviewAdmin(VersionAdmin, PolymorphicChildModelAdmin):
    list_display = ('__str__', 'number', 'year', 'month', 'day', 'double',)
    search_fields = ('title', 'number', 'year',)
    show_in_index = True


class BorrowAdmin(VersionAdmin):
    list_display = ('borrowable', 'user', 'borrow_date', 'borrowed_with',
                    'given_back_to')
    search_fields = ('borrowable__isbn', 'borrowable__title',
                     'borrowable__medium__side_identifier',
                     'user__username', 'borrow_date', 'given_back')
    date_hierarchy = 'borrow_date'
    autocomplete_fields = ('borrowable', 'user', 'borrowed_with',
                           'given_back_to')

    def add_view(self, request, form_url='', extra_context=None):
        """
        Autoselect keyholder registering a new borrowed item
        """
        # Make GET data mutable
        data = request.GET.copy()
        data['borrowed_with'] = request.user
        request.GET = data
        return super().add_view(request, form_url, extra_context)


class GameAdmin(VersionAdmin, PolymorphicChildModelAdmin):
    list_display = ('title', 'owner', 'duration', 'players_min',
                    'players_max', 'comment', 'isbn')
    search_fields = ('isbn', 'title', 'owner__username', 'duration', 'comment')
    autocomplete_fields = ('owner',)
    show_in_index = True


admin_site.register(Author, AuthorAdmin)
admin_site.register(Borrowable, BorrowableAdmin)
admin_site.register(Comic, MediumAdmin)
admin_site.register(Manga, MediumAdmin)
admin_site.register(Novel, MediumAdmin)
admin_site.register(CD, CDAdmin)
admin_site.register(Vinyl, VinylAdmin)
admin_site.register(Review, ReviewAdmin)
admin_site.register(FutureMedium, FutureMediumAdmin)
admin_site.register(Borrow, BorrowAdmin)
admin_site.register(Game, GameAdmin)
