# -*- mode: python; coding: utf-8 -*-
# Copyright (C) 2017-2019 by BDE ENS Paris-Saclay
# SPDX-License-Identifier: GPL-3.0-or-later

from django.urls import path

from . import views

app_name = 'media'
urlpatterns = [
    path('find/', views.FindMediumView.as_view(), name="find"),
    path('mark-as-present/comic/<int:pk>/',
         views.MarkComicAsPresent.as_view(),
         name="mark_comic_as_present"),
    path('mark-as-present/manga/<int:pk>/',
         views.MarkMangaAsPresent.as_view(),
         name="mark_manga_as_present"),
    path('mark-as-present/cd/<int:pk>/',
         views.MarkCDAsPresent.as_view(),
         name="mark_cd_as_present"),
    path('mark-as-present/vinyl/<int:pk>/',
         views.MarkVinylAsPresent.as_view(),
         name="mark_vinyle_as_present"),
    path('mark-as-present/novel/<int:pk>/',
         views.MarkNovelAsPresent.as_view(),
         name="mark_novel_as_present"),
    path('mark-as-present/review/<int:pk>/',
         views.MarkReviewAsPresent.as_view(),
         name="mark_review_as_present"),
    path('mark-as-present/future/<int:pk>/',
         views.MarkFutureAsPresent.as_view(),
         name="mark_future_as_present"),
]
