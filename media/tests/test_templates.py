# -*- mode: python; coding: utf-8 -*-
# SPDX-License-Identifier: GPL-3.0-or-later

from django.test import TestCase
from django.urls import reverse
from media.models import Author, Comic
from users.models import User

"""
Test that every page render
"""


class TemplateTests(TestCase):
    def setUp(self):
        self.user = User.objects.create_superuser(
            username="admin",
            password="adminadmin",
            email="admin@example.com",
        )
        self.client.force_login(self.user)

        # Create an author
        self.dummy_author = Author.objects.create(name="Test author")

        # Create media
        self.dummy_bd1 = Comic.objects.create(
            title="Test media",
            side_identifier="T M",
        )
        self.dummy_bd1.authors.add(self.dummy_author)
        self.dummy_bd2 = Comic.objects.create(
            title="Test media bis",
            side_identifier="T M 2",
            external_url="https://example.com/",
        )
        self.dummy_bd2.authors.add(self.dummy_author)

    def test_comic_bd_changelist(self):
        response = self.client.get(reverse('admin:media_comic_changelist'))
        self.assertEqual(response.status_code, 200)

    def test_comic_bd_add(self):
        response = self.client.get(reverse('admin:media_comic_add'))
        self.assertEqual(response.status_code, 200)

    def test_comic_isbn_download(self):
        data = {
            '_isbn': True,
            'isbn': "0316358525",
        }
        response = self.client.post(reverse(
            'admin:media_comic_change',
            args=[self.dummy_bd1.id],
        ), data=data)
        self.assertEqual(response.status_code, 302)

    def test_comic_borrow_changelist(self):
        response = self.client.get(reverse('admin:media_borrow_changelist'))
        self.assertEqual(response.status_code, 200)

    def test_comic_borrow_add(self):
        response = self.client.get(reverse('admin:media_borrow_add'))
        self.assertEqual(response.status_code, 200)
