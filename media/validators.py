# -*- mode: python; coding: utf-8 -*-
# Copyright (C) 2017-2019 by BDE ENS Paris-Saclay
# SPDX-License-Identifier: GPL-3.0-or-later

"""
Based on https://github.com/secnot/django-isbn-field
"""

from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _


def isbn_validator(raw_isbn):
    """Check string is a valid ISBN number"""
    isbn_to_check = raw_isbn.replace('-', '').replace(' ', '')

    if not isinstance(isbn_to_check, str):
        raise ValidationError(_('Invalid ISBN: Not a string'))

    if len(isbn_to_check) != 10 and len(isbn_to_check) != 13:
        raise ValidationError(_('Invalid ISBN: Wrong length'))

    # if not isbn.is_valid(isbn_to_check):
    #    raise ValidationError(_('Invalid ISBN: Failed checksum'))

    if isbn_to_check != isbn_to_check.upper():
        raise ValidationError(_('Invalid ISBN: Only upper case allowed'))

    return True
