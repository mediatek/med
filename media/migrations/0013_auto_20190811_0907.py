# Generated by Django 2.2.4 on 2019-08-11 07:07

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('media', '0012_media_external_url'),
    ]

    operations = [
        migrations.RenameField(
            model_name='media',
            old_name='auteur',
            new_name='authors',
        ),
    ]
