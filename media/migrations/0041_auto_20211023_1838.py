# Generated by Django 2.2.17 on 2021-10-23 16:38

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('media', '0040_auto_20211023_1830'),
    ]

    operations = [
        migrations.RenameField(
            model_name='game',
            old_name='duree',
            new_name='duration',
        ),
        migrations.RenameField(
            model_name='game',
            old_name='proprietaire',
            new_name='owner',
        ),
        migrations.RenameField(
            model_name='game',
            old_name='nombre_joueurs_max',
            new_name='players_max',
        ),
        migrations.RenameField(
            model_name='game',
            old_name='nombre_joueurs_min',
            new_name='players_min',
        ),
        migrations.AlterField(
            model_name='emprunt',
            name='media',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='media.Comic'),
        ),
        migrations.AlterField(
            model_name='futuremedium',
            name='type',
            field=models.CharField(choices=[('bd', 'Comic'), ('manga', 'Manga'), ('roman', 'Roman')], max_length=8, verbose_name='type'),
        ),
    ]
