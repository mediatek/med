# Generated by Django 2.2.4 on 2020-02-10 16:09

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('media', '0024_auto_20190816_1356'),
    ]

    operations = [
        migrations.AddField(
            model_name='auteur',
            name='note',
            field=models.IntegerField(default=0, verbose_name='note'),
        ),
    ]
