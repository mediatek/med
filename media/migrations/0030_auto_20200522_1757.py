# Generated by Django 2.2.10 on 2020-05-22 15:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('media', '0029_auto_20200521_1659'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Media',
            new_name='BD',
        ),
        migrations.AlterModelOptions(
            name='manga',
            options={},
        ),
        migrations.CreateModel(
            name='Vinyle',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=255, verbose_name='title')),
                ('side_identifier', models.CharField(max_length=255, verbose_name='side identifier')),
                ('authors', models.ManyToManyField(to='media.Auteur', verbose_name='authors')),
            ],
            options={
                'verbose_name': 'vinyle',
                'verbose_name_plural': 'vinyles',
                'ordering': ['title'],
            },
        ),
        migrations.CreateModel(
            name='CD',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=255, verbose_name='title')),
                ('side_identifier', models.CharField(max_length=255, verbose_name='side identifier')),
                ('authors', models.ManyToManyField(to='media.Auteur', verbose_name='authors')),
            ],
            options={
                'verbose_name': 'CD',
                'verbose_name_plural': 'CDs',
                'ordering': ['title'],
            },
        ),
    ]
