# -*- mode: python; coding: utf-8 -*-
# Copyright (C) 2017-2021 by BDE ENS Paris-Saclay
# SPDX-License-Identifier: GPL-3.0-or-later
from django.conf import settings
from django.core.validators import MinValueValidator
from django.db import models
from django.utils.translation import gettext_lazy as _
from polymorphic.models import PolymorphicModel

from .fields import ISBNField


class Author(models.Model):
    name = models.CharField(
        max_length=255,
        unique=True,
        verbose_name=_('name'),
    )

    note = models.IntegerField(
        default=0,
        verbose_name=_("note"),
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _("author")
        verbose_name_plural = _("authors")
        ordering = ['name']


class Borrowable(PolymorphicModel):
    isbn = ISBNField(
        _('ISBN'),
        help_text=_('You may be able to scan it from a bar code.'),
        unique=True,
        blank=True,
        null=True,
    )

    title = models.CharField(
        max_length=255,
        verbose_name=_("title"),
    )

    present = models.BooleanField(
        verbose_name=_("present"),
        help_text=_("Tell that the medium is present in the Mediatek."),
        default=False,
    )

    def __str__(self):
        obj = self
        if obj.__class__ == Borrowable:
            # Get true object instance, useful for autocompletion
            obj = Borrowable.objects.get(pk=obj.pk)

        title = obj.title
        if hasattr(obj, 'subtitle'):
            subtitle = obj.subtitle
            if subtitle:
                title = f"{title} : {subtitle}"
        return title

    class Meta:
        verbose_name = _('borrowable')
        verbose_name_plural = _('borrowables')


class Medium(Borrowable):
    external_url = models.URLField(
        verbose_name=_('external URL'),
        blank=True,
    )

    side_identifier = models.CharField(
        verbose_name=_('side identifier'),
        max_length=255,
    )

    authors = models.ManyToManyField(
        'Author',
        verbose_name=_('authors'),
    )

    class Meta:
        verbose_name = _("medium")
        verbose_name_plural = _("media")


class Book(Medium):
    subtitle = models.CharField(
        verbose_name=_('subtitle'),
        max_length=255,
        blank=True,
    )

    number_of_pages = models.PositiveIntegerField(
        verbose_name=_('number of pages'),
        blank=True,
        null=True,
    )

    publish_date = models.DateField(
        verbose_name=_('publish date'),
        blank=True,
        null=True,
    )

    class Meta:
        verbose_name = _("book")
        verbose_name_plural = _("books")


class Comic(Book):
    class Meta:
        verbose_name = _("comic")
        verbose_name_plural = _("comics")
        ordering = ['title', 'subtitle']


class Manga(Book):
    class Meta:
        verbose_name = _("manga")
        verbose_name_plural = _("mangas")
        ordering = ['title', 'subtitle']


class Novel(Book):
    class Meta:
        verbose_name = _("novel")
        verbose_name_plural = _("novels")
        ordering = ['title', 'subtitle']


class Vinyl(Medium):
    rpm = models.PositiveIntegerField(
        verbose_name=_('rounds per minute'),
        choices=[
            (33, _('33 RPM')),
            (45, _('45 RPM')),
        ],
    )

    class Meta:
        verbose_name = _("vinyl")
        verbose_name_plural = _("vinyls")
        ordering = ['title']


class CD(Medium):
    class Meta:
        verbose_name = _("CD")
        verbose_name_plural = _("CDs")
        ordering = ['title']


class Review(Borrowable):
    number = models.PositiveIntegerField(
        verbose_name=_('number'),
    )

    year = models.PositiveIntegerField(
        verbose_name=_('year'),
        null=True,
        blank=True,
        default=None,
    )

    month = models.PositiveIntegerField(
        verbose_name=_('month'),
        null=True,
        blank=True,
        default=None,
    )

    day = models.PositiveIntegerField(
        verbose_name=_('day'),
        null=True,
        blank=True,
        default=None,
    )

    double = models.BooleanField(
        verbose_name=_('double'),
        default=False,
    )

    def __str__(self):
        return self.title + " n°" + str(self.number)

    class Meta:
        verbose_name = _("review")
        verbose_name_plural = _("reviews")
        ordering = ['title', 'number']


class FutureMedium(models.Model):
    isbn = ISBNField(
        _('ISBN'),
        help_text=_('You may be able to scan it from a bar code.'),
        unique=True,
        blank=True,
        null=True,
    )

    type = models.CharField(
        _('type'),
        choices=[
            ('bd', _('Comic')),
            ('manga', _('Manga')),
            ('roman', _('Roman')),
        ],
        max_length=8,
    )

    present = models.BooleanField(
        verbose_name=_("present"),
        help_text=_("Tell that the medium is present in the Mediatek."),
        default=False,
    )

    class Meta:
        verbose_name = _("future medium")
        verbose_name_plural = _("future media")

    def __str__(self):
        return "Future medium (ISBN: {isbn})".format(isbn=self.isbn, )


class Borrow(models.Model):
    borrowable = models.ForeignKey(
        'media.Borrowable',
        on_delete=models.PROTECT,
        verbose_name=_('object'),
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.PROTECT,
        verbose_name=_("borrower"),
    )
    borrow_date = models.DateTimeField(
        verbose_name=_('borrowed on'),
    )
    given_back = models.DateTimeField(
        blank=True,
        null=True,
        verbose_name=_('given back on'),
    )
    borrowed_with = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.PROTECT,
        related_name='+',
        verbose_name=_('borrowed with'),
        help_text=_('The keyholder that registered this borrowed item.')
    )
    given_back_to = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.PROTECT,
        related_name='+',
        blank=True,
        null=True,
        verbose_name=_('given back to'),
        help_text=_('The keyholder to whom this item was given back.')
    )

    def __str__(self):
        return str(self.borrowable) + str(self.user)

    class Meta:
        verbose_name = _("borrowed item")
        verbose_name_plural = _("borrowed items")
        ordering = ['-borrow_date']


class Game(Borrowable):
    DURATIONS = (
        ('-1h', '-1h'),
        ('1-2h', '1-2h'),
        ('2-3h', '2-3h'),
        ('3-4h', '3-4h'),
        ('4h+', '4h+'),
    )
    owner = models.ForeignKey(
        'users.User',
        on_delete=models.PROTECT,
        verbose_name=_("owner"),
    )
    duration = models.CharField(
        choices=DURATIONS,
        max_length=255,
        verbose_name=_("duration"),
    )
    players_min = models.IntegerField(
        validators=[MinValueValidator(1)],
        verbose_name=_("minimum number of players"),
    )
    players_max = models.IntegerField(
        validators=[MinValueValidator(1)],
        verbose_name=_('maximum number of players'),
    )
    comment = models.CharField(
        max_length=255,
        blank=True,
        verbose_name=_('comment'),
    )

    def __str__(self):
        return str(self.title)

    class Meta:
        verbose_name = _("game")
        verbose_name_plural = _("games")
        ordering = ['title']
