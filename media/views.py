# -*- mode: python; coding: utf-8 -*-
# Copyright (C) 2017-2019 by BDE ENS Paris-Saclay
# SPDX-License-Identifier: GPL-3.0-or-later

from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponse
from django_filters.rest_framework import DjangoFilterBackend
from django.shortcuts import redirect
from django.utils.translation import gettext_lazy as _
from django.views.generic import TemplateView, DetailView
from rest_framework import viewsets
from rest_framework.filters import SearchFilter

from .models import Author, Borrow, CD, Comic, FutureMedium, Game, Manga,\
    Novel, Review, Vinyl
from .serializers import AuthorSerializer, BorrowSerializer, ComicSerializer, \
    CDSerializer, FutureMediumSerializer, GameSerializer, MangaSerializer, \
    NovelSerializer, ReviewSerializer, VinylSerializer


class IndexView(TemplateView):
    """
    Home page which redirect to admin when logged in
    """
    extra_context = {'title': _('Welcome to the Mediatek database')}
    template_name = 'admin/index.html'

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return redirect('admin:index')
        return super().dispatch(request, *args, **kwargs)


class FindMediumView(LoginRequiredMixin, TemplateView):
    template_name = "media/find_medium.html"


class MarkMediumAsPresent(LoginRequiredMixin, DetailView):
    def dispatch(self, request, *args, **kwargs):
        object = self.get_object()
        object.present = not request.GET.get("absent", False)
        object.save()
        return HttpResponse("", content_type=204)


class MarkComicAsPresent(MarkMediumAsPresent):
    model = Comic


class MarkMangaAsPresent(MarkMediumAsPresent):
    model = Manga


class MarkCDAsPresent(MarkMediumAsPresent):
    model = CD


class MarkVinylAsPresent(MarkMediumAsPresent):
    model = Vinyl


class MarkNovelAsPresent(MarkMediumAsPresent):
    model = Novel


class MarkReviewAsPresent(MarkMediumAsPresent):
    model = Review


class MarkFutureAsPresent(MarkMediumAsPresent):
    model = FutureMedium


class AuthorViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows authors to be viewed or edited.
    """
    queryset = Author.objects.all()
    serializer_class = AuthorSerializer


class ComicViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows media to be viewed or edited.
    """
    queryset = Comic.objects.all()
    serializer_class = ComicSerializer
    filter_backends = [DjangoFilterBackend, SearchFilter]
    filterset_fields = ["isbn", "side_identifier"]
    search_fields = ["=isbn", "title", "subtitle", "side_identifier",
                     "authors__name"]


class MangaViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows media to be viewed or edited.
    """
    queryset = Manga.objects.all()
    serializer_class = MangaSerializer
    filter_backends = [DjangoFilterBackend, SearchFilter]
    filterset_fields = ["isbn", "side_identifier"]
    search_fields = ["=isbn", "title", "subtitle", "side_identifier",
                     "authors__name"]


class CDViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows media to be viewed or edited.
    """
    queryset = CD.objects.all()
    serializer_class = CDSerializer
    filter_backends = [DjangoFilterBackend, SearchFilter]
    filterset_fields = ["side_identifier"]
    search_fields = ["title", "side_identifier", "authors__name"]


class VinylViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows media to be viewed or edited.
    """
    queryset = Vinyl.objects.all()
    serializer_class = VinylSerializer
    filter_backends = [DjangoFilterBackend, SearchFilter]
    filterset_fields = ["side_identifier", "rpm"]
    search_fields = ["title", "side_identifier", "authors__name"]


class NovelViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows media to be viewed or edited.
    """
    queryset = Novel.objects.all()
    serializer_class = NovelSerializer
    filter_backends = [DjangoFilterBackend, SearchFilter]
    filterset_fields = ["isbn", "side_identifier", "number_of_pages"]
    search_fields = ["=isbn", "title", "subtitle", "side_identifier",
                     "authors__name"]


class ReviewViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows media to be viewed or edited.
    """
    queryset = Review.objects.all()
    serializer_class = ReviewSerializer
    filter_backends = [DjangoFilterBackend, SearchFilter]
    filterset_fields = ["number", "year", "month", "day", "double"]
    search_fields = ["title"]


class FutureMediumViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows media to be viewed or edited.
    """
    queryset = FutureMedium.objects.all()
    serializer_class = FutureMediumSerializer
    filter_backends = [DjangoFilterBackend, SearchFilter]
    filterset_fields = ["isbn"]
    search_fields = ["=isbn"]


class BorrowViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows borrowed items to be viewed or edited.
    """
    queryset = Borrow.objects.all()
    serializer_class = BorrowSerializer


class GameViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows games to be viewed or edited.
    """
    queryset = Game.objects.all()
    serializer_class = GameSerializer
